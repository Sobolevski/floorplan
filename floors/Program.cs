﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using floorplan.Models;
using System.Diagnostics;


namespace floorplan
{
    public class Program
    {
        public static void Main(List<Apartment> apartments)
        {
            const int HEIGHT = 21500;
            const int WIDTH = 21000;
            const int DOOR_WIDTH = 900;

            foreach (Apartment apartment in apartments)
            {
                foreach (List<Wall> room in apartment.rooms)
                {
                    foreach (Wall wall in room)
                    {
                        //Check whether you need a window in the wall
                        if (wall.fromPoint.x == wall.toPoint.x)
                        {
                            if (wall.fromPoint.x == 0 || wall.fromPoint.x == WIDTH)
                            {
                                wall.window = true;
                            }
                        }
                        else
                        {
                            if (wall.fromPoint.y == 0 || wall.fromPoint.y == HEIGHT)
                            {
                                wall.window = true;
                            }
                        }

                        if (wall.window == false)
                        {
                            foreach (Wall apart in apartment.border)
                            {
                                if (wall.fromPoint.x == wall.toPoint.x)
                                {
                                    int start = apart.fromPoint.y > apart.toPoint.y ? apart.toPoint.y : apart.fromPoint.y;
                                    int end = apart.fromPoint.y > apart.toPoint.y ? apart.fromPoint.y : apart.toPoint.y;
                                    if (wall.fromPoint.x == apart.fromPoint.x && (start <= wall.fromPoint.y && wall.fromPoint.y <= end)
                                        && (start <= wall.toPoint.y && wall.toPoint.y <= end))
                                    {
                                        wall.isBorderWall = true;

                                    }
                                }
                                else
                                {
                                    int start = apart.fromPoint.x > apart.toPoint.x ? apart.toPoint.x : apart.fromPoint.x;
                                    int end = apart.fromPoint.x > apart.toPoint.x ? apart.fromPoint.x : apart.toPoint.x;
                                    if (wall.fromPoint.y == apart.fromPoint.y && ((start <= wall.fromPoint.x && wall.fromPoint.x <= end)
                                        && (start <= wall.toPoint.x && wall.toPoint.x <= end)))

                                    {
                                        wall.isBorderWall = true;
                                    }
                                }
                            }

                        }

                        //find end points in wall

                        if (wall.isBorderWall == false && wall.window == false)
                        {
                            if (wall.fromPoint.y == wall.toPoint.y)
                            {
                                var findPoints = (from apart in apartments
                                                  from rooms in apart.rooms
                                                  from line in rooms
                                                  where line.fromPoint.x == line.toPoint.x
                                                  where line.fromPoint.y == wall.fromPoint.y || line.toPoint.y == wall.fromPoint.y
                                                  where (wall.fromPoint.x < line.fromPoint.x && line.fromPoint.x < wall.toPoint.x) ||
                                                  (wall.toPoint.x < line.fromPoint.x && line.fromPoint.x < wall.fromPoint.x)
                                                  select line.fromPoint.x).Distinct();

                                foreach (var point in findPoints)
                                {
                                    wall.partitionWall.Add(point);
                                }
                            }
                            else if (wall.fromPoint.x == wall.toPoint.x)
                            {
                                var findPoints = (from apart in apartments
                                                  from rooms in apart.rooms
                                                  from line in rooms
                                                  where line.fromPoint.y == line.toPoint.y
                                                  where line.fromPoint.x == wall.fromPoint.x || line.toPoint.x == wall.fromPoint.x
                                                  where (wall.fromPoint.y < line.fromPoint.y && line.fromPoint.y < wall.toPoint.y) ||
                                                  (wall.toPoint.y < line.fromPoint.y && line.fromPoint.y < wall.fromPoint.y)
                                                  select line.fromPoint.y).Distinct();


                                foreach (var point in findPoints)
                                {
                                    wall.partitionWall.Add(point);
                                }
                            }

                        }
                    }

                }

            }
            foreach (var apartment in apartments)
            {
                var findNotUnionBorder = (from border in apartment.border
                                          where (border.fromPoint.x != 0 && border.fromPoint.y != 0 && border.toPoint.x != 0 && border.toPoint.y != 0 && border.fromPoint.x != WIDTH && border.toPoint.x != WIDTH
                                                && border.fromPoint.y != HEIGHT && border.toPoint.y != HEIGHT)
                                          select border).Distinct().ToList();
                int count = 0;
                do
                {
                    Wall border = findNotUnionBorder[count];
                    if (border.fromPoint.y == border.toPoint.y)
                    {

                        int start = border.fromPoint.x > border.toPoint.x ? border.toPoint.x : border.fromPoint.x;
                        int end = border.fromPoint.x > border.toPoint.x ? border.fromPoint.x : border.toPoint.x;

                        if (end - start > DOOR_WIDTH)
                        {
                            var findPartition = (from rooms in apartment.rooms
                                                 from room in rooms
                                                 where room.fromPoint.x == room.toPoint.x
                                                 where room.fromPoint.y == border.fromPoint.y || border.fromPoint.y == room.toPoint.y
                                                 where (border.fromPoint.x < room.fromPoint.x && room.fromPoint.x < border.toPoint.x) ||
                                                   (border.toPoint.x < room.fromPoint.x && room.fromPoint.x < border.fromPoint.x)
                                                 select room.fromPoint.x).Distinct().ToList();
                            findPartition.Sort();
                            if (findPartition.Count != 0)
                            {
                                foreach (var point in findPartition)
                                {
                                    if (end - point > DOOR_WIDTH)
                                    {
                                        border.EntranceDoor = true;
                                        border.partitionWall.Add(point);

                                        break;
                                    }
                                    else
                                    {
                                        continue;
                                    }

                                }
                            }
                            else
                            {
                                border.partitionWall.Add(end);
                                border.EntranceDoor = true;
                            }
                        }
                    }
                    else if (border.fromPoint.x == border.toPoint.x)
                    {
                        int start = border.fromPoint.y > border.toPoint.y ? border.toPoint.y : border.fromPoint.y;
                        int end = border.fromPoint.y > border.toPoint.y ? border.fromPoint.y : border.toPoint.y;

                        if (end - start > DOOR_WIDTH)
                        {
                            var findPartition = (from rooms in apartment.rooms
                                                 from room in rooms
                                                 where room.fromPoint.y == room.toPoint.y
                                                 where room.fromPoint.x == border.fromPoint.x || border.fromPoint.x == room.toPoint.x
                                                 where (border.fromPoint.y < room.fromPoint.y && room.fromPoint.y < border.toPoint.y) ||
                                                   (border.toPoint.y < room.fromPoint.y && room.fromPoint.y < border.fromPoint.y)
                                                 select room.fromPoint.y).Distinct().ToList();

                            findPartition.Sort();
                            if (findPartition.Count != 0)
                            {
                                foreach (var point in findPartition)
                                {
                                    if (end - point > DOOR_WIDTH)
                                    {
                                        border.EntranceDoor = true;
                                        border.partitionWall.Add(point);
                                        break;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                            }
                            else
                            {
                                border.partitionWall.Add(end);
                                border.EntranceDoor = true;
                            }
                        }
                    }
                }
                while (apartment.EntranceDoor == true);
            }
        }
    }
}


