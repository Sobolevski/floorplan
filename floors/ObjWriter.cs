﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using floorplan.Models;

namespace floorplan
{
    public class ObjWriter

    {
        #region Constants
        const string FILENAME = "floorplan.obj";

        const int HEIGHT = 21500;
        const int WIDTH = 21000;
        const int FLOOR_HEIGHT = 3000;
        const int DOOR_HEIGHT = 2100;
        const int DOOR_WIDTH = 900;
        const int WINDOW_HEIGHT = 1500;
        const int WINDOW_WIDTH = 1500;        
        const int WINDOWSSILL_HEIGHT = (FLOOR_HEIGHT - WINDOW_HEIGHT) / 2;
        const int WINDOWS_POINTS = 10;
        const int DOOR_POINTS = 8;
        const int WALL_POINTS = 4;
        #endregion

        private static int DrawWall(StreamWriter sr, Wall wall, int counter)
        {
            if (wall.fromPoint.y == wall.toPoint.y && wall.fromPoint.y != 0 && wall.toPoint.y != HEIGHT)
            {
                sr.WriteLine(string.Format("v {0} {1} 0", wall.fromPoint.x, wall.fromPoint.y));
                sr.WriteLine(string.Format("v {0} {1} 0", wall.toPoint.x, wall.fromPoint.y));
                sr.WriteLine(string.Format("v {0} {1} {2}", wall.toPoint.x, wall.fromPoint.y, FLOOR_HEIGHT));
                sr.WriteLine(string.Format("v {0} {1} {2}", wall.fromPoint.x, wall.fromPoint.y, FLOOR_HEIGHT));

                counter = WriteUnionPoint(sr, WALL_POINTS, counter);
            }
            else if (wall.fromPoint.x != 0 && wall.toPoint.x != WIDTH)
            {
                sr.WriteLine(string.Format("v {0} {1} 0 ", wall.fromPoint.x, wall.fromPoint.y));
                sr.WriteLine(string.Format("v {0} {1} 0  ", wall.fromPoint.x, wall.toPoint.y));
                sr.WriteLine(string.Format("v {0} {1} {2}", wall.fromPoint.x, wall.toPoint.y, FLOOR_HEIGHT));
                sr.WriteLine(string.Format("v {0} {1} {2}", wall.fromPoint.x, wall.fromPoint.y, FLOOR_HEIGHT));

                counter = WriteUnionPoint(sr, WALL_POINTS, counter);
            }

            return counter;
        }

        private static int WriteUnionPoint(StreamWriter sr, int points, int counter)
        {
            sr.Write("f ");
            for (int i = 0; i < points; i++)
            {
                sr.Write(counter++ + " ");
            }
            sr.WriteLine();

            return counter;
        }

        public static void WriteApartment(List<Apartment> apartments)
        {
            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            string filePath = Path.Combine(desktopPath, FILENAME);

            int counter = 1;

            using (StreamWriter sr = new StreamWriter(filePath, false, System.Text.Encoding.Default))
            {
                
                foreach (Apartment apartment in apartments)
                {                   
                    foreach(Wall wallBorder in apartment.border)
                    {
                        if (wallBorder.EntranceDoor == true)
                        {
                            if(wallBorder.fromPoint.y == wallBorder.toPoint.y)
                            {
                                int start = wallBorder.fromPoint.x > wallBorder.toPoint.x ? wallBorder.toPoint.x : wallBorder.fromPoint.x;
                                int end = wallBorder.fromPoint.x > wallBorder.toPoint.x ? wallBorder.fromPoint.x : wallBorder.toPoint.x;

                                int Y = wallBorder.fromPoint.y;
                                int point = wallBorder.partitionWall[0];
                                int offset = (point - start - DOOR_WIDTH) / 2;

                                sr.WriteLine(string.Format("v {0} {1} 0", start, Y));
                                sr.WriteLine(string.Format("v {0} {1} 0", start + offset, Y));
                                sr.WriteLine(string.Format("v {0} {1} {2}", start + offset, Y, DOOR_HEIGHT));
                                sr.WriteLine(string.Format("v {0} {1} {2}", start + offset + DOOR_WIDTH, Y, DOOR_HEIGHT));
                                sr.WriteLine(string.Format("v {0} {1} 0", start + offset + DOOR_WIDTH, Y));
                                sr.WriteLine(string.Format("v {0} {1} 0", end, Y));
                                sr.WriteLine(string.Format("v {0} {1} {2}", end, Y, FLOOR_HEIGHT));
                                sr.WriteLine(string.Format("v {0} {1} {2}", start, Y, FLOOR_HEIGHT));

                                counter = WriteUnionPoint(sr, DOOR_POINTS, counter);

                            }
                            else if(wallBorder.fromPoint.x == wallBorder.toPoint.x)
                            {

                                int start = wallBorder.fromPoint.y > wallBorder.toPoint.y ? wallBorder.toPoint.y : wallBorder.fromPoint.y;
                                int end = wallBorder.fromPoint.y > wallBorder.toPoint.y ? wallBorder.fromPoint.y : wallBorder.toPoint.y;

                                int X = wallBorder.fromPoint.x;

                                int point = wallBorder.partitionWall[0];
                                int offset = (point - start - DOOR_WIDTH) / 2;

                                sr.WriteLine(string.Format("v {0} {1} 0", X, start)); 
                                sr.WriteLine(string.Format("v {0} {1} 0", X, start + offset));
                                sr.WriteLine(string.Format("v {0} {1} {2}", X, start + offset, DOOR_HEIGHT));
                                sr.WriteLine(string.Format("v {0} {1} {2}", X, start + offset + DOOR_WIDTH, DOOR_HEIGHT));
                                sr.WriteLine(string.Format("v {0} {1} 0", X, start + offset + DOOR_WIDTH));
                                sr.WriteLine(string.Format("v {0} {1} 0", X, end));
                                sr.WriteLine(string.Format("v {0} {1} {2}", X, end, FLOOR_HEIGHT));
                                sr.WriteLine(string.Format("v {0} {1} {2}", X, start, FLOOR_HEIGHT));

                                counter = WriteUnionPoint(sr, DOOR_POINTS, counter);
                            }
                        }
                        else
                        {
                            counter = DrawWall(sr, wallBorder, counter);
                        }
                    }
                    
                    foreach (List<Wall> room in apartment.rooms)
                    {
                        foreach (Wall wall in room)
                        {
                            if (wall.window == true)
                            {
                                if (wall.fromPoint.y == wall.toPoint.y)
                                {
                                    int width = wall.fromPoint.x > wall.toPoint.x ? -((wall.fromPoint.x - wall.toPoint.x) - WINDOW_WIDTH) / 2 : ((wall.toPoint.x - wall.fromPoint.x) - WINDOW_WIDTH) / 2;

                                    int Y = wall.fromPoint.y;

                                    sr.WriteLine(string.Format("v {0} {1} 0", wall.fromPoint.x, Y));
                                    sr.WriteLine(string.Format("v {0} {1} 0", wall.toPoint.x, Y));
                                    sr.WriteLine(string.Format("v {0} {1} {2}", wall.toPoint.x, Y, FLOOR_HEIGHT));
                                    sr.WriteLine(string.Format("v {0} {1} {2}", wall.toPoint.x - width, Y, FLOOR_HEIGHT));
                                    sr.WriteLine(string.Format("v {0} {1} {2}", wall.toPoint.x - width, Y, FLOOR_HEIGHT - WINDOW_HEIGHT - WINDOWSSILL_HEIGHT));
                                    sr.WriteLine(string.Format("v {0} {1} {2}", wall.fromPoint.x + width, Y, FLOOR_HEIGHT - WINDOW_HEIGHT - WINDOWSSILL_HEIGHT));
                                    sr.WriteLine(string.Format("v {0} {1} {2}", wall.fromPoint.x + width, Y, FLOOR_HEIGHT - WINDOWSSILL_HEIGHT));
                                    sr.WriteLine(string.Format("v {0} {1} {2}", wall.toPoint.x - width, Y, FLOOR_HEIGHT - WINDOWSSILL_HEIGHT));
                                    sr.WriteLine(string.Format("v {0} {1} {2}", wall.toPoint.x - width, Y, FLOOR_HEIGHT));
                                    sr.WriteLine(string.Format("v {0} {1} {2}", wall.fromPoint.x, Y, FLOOR_HEIGHT));

                                    counter = WriteUnionPoint(sr, WINDOWS_POINTS, counter);
                                }
                                else if (wall.fromPoint.x == wall.toPoint.x)
                                {
                                    int width = wall.fromPoint.y > wall.toPoint.y ? -((wall.fromPoint.y - wall.toPoint.y) - WINDOW_WIDTH) / 2 : ((wall.toPoint.y - wall.fromPoint.y) - WINDOW_WIDTH) / 2;

                                    int X = wall.fromPoint.x;

                                    sr.WriteLine(string.Format("v {0} {1} 0", X, wall.fromPoint.y));
                                    sr.WriteLine(string.Format("v {0} {1} 0", X, wall.toPoint.y));
                                    sr.WriteLine(string.Format("v {0} {1} {2}", X, wall.toPoint.y, FLOOR_HEIGHT));
                                    sr.WriteLine(string.Format("v {0} {1} {2}", X, wall.toPoint.y - width, FLOOR_HEIGHT));
                                    sr.WriteLine(string.Format("v {0} {1} {2}", X, wall.toPoint.y - width, FLOOR_HEIGHT - WINDOW_HEIGHT - WINDOWSSILL_HEIGHT));
                                    sr.WriteLine(string.Format("v {0} {1} {2}", X, wall.fromPoint.y + width, FLOOR_HEIGHT - WINDOW_HEIGHT - WINDOWSSILL_HEIGHT));
                                    sr.WriteLine(string.Format("v {0} {1} {2}", X, wall.fromPoint.y + width, FLOOR_HEIGHT - WINDOWSSILL_HEIGHT));
                                    sr.WriteLine(string.Format("v {0} {1} {2}", X, wall.toPoint.y - width, FLOOR_HEIGHT - WINDOWSSILL_HEIGHT));
                                    sr.WriteLine(string.Format("v {0} {1} {2}", X, wall.toPoint.y - width, FLOOR_HEIGHT));
                                    sr.WriteLine(string.Format("v {0} {1} {2}", X, wall.fromPoint.y, FLOOR_HEIGHT));

                                    counter = WriteUnionPoint(sr, WINDOWS_POINTS, counter);
                                }
                            }

                            else if ((wall.window == false && wall.isBorderWall == false))
                            {                              
                                if (wall.fromPoint.y == wall.toPoint.y)
                                {
                                    int start = wall.fromPoint.x > wall.toPoint.x ? wall.toPoint.x : wall.fromPoint.x;
                                    int end = wall.fromPoint.x > wall.toPoint.x ? wall.fromPoint.x : wall.toPoint.x;

                                    int Y = wall.fromPoint.y;
                                    
                                    if(wall.partitionWall.Count != 0)
                                    {
                                        wall.partitionWall.Sort();
                                        foreach(int point in wall.partitionWall)
                                        {
                                            if(point - start > DOOR_WIDTH)
                                            {
                                                sr.WriteLine(string.Format("v {0} {1} 0", start, Y));
                                                int offset = (point - start - DOOR_WIDTH) / 2;
                                                sr.WriteLine(string.Format("v {0} {1} 0", start + offset, Y));
                                                sr.WriteLine(string.Format("v {0} {1} {2}", start + offset, Y, DOOR_HEIGHT));
                                                sr.WriteLine(string.Format("v {0} {1} {2}", start + offset + DOOR_WIDTH, Y, DOOR_HEIGHT));
                                                sr.WriteLine(string.Format("v {0} {1} 0", start + offset + DOOR_WIDTH, Y));
                                                sr.WriteLine(string.Format("v {0} {1} 0", point, Y));
                                                sr.WriteLine(string.Format("v {0} {1} {2}", point, Y, FLOOR_HEIGHT));
                                                sr.WriteLine(string.Format("v {0} {1} {2}", start, Y, FLOOR_HEIGHT));

                                                start = point;

                                                counter = WriteUnionPoint(sr, DOOR_POINTS, counter);
                                            }
                                            else if(DOOR_WIDTH > point - start)
                                            {
                                                counter = DrawWall(sr, new Wall(new Point(start, Y), new Point(point, Y)), counter);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (end - start > DOOR_WIDTH)
                                        {
                                            int offset = (end - start - DOOR_WIDTH) / 2;
                                            sr.WriteLine(string.Format("v {0} {1} 0", start, Y));
                                            sr.WriteLine(string.Format("v {0} {1} 0", start + offset, Y));
                                            sr.WriteLine(string.Format("v {0} {1} {2}", start + offset, Y, DOOR_HEIGHT));
                                            sr.WriteLine(string.Format("v {0} {1} {2}", start + offset + DOOR_WIDTH, Y, DOOR_HEIGHT));
                                            sr.WriteLine(string.Format("v {0} {1} 0", start + offset + DOOR_WIDTH, Y));
                                            sr.WriteLine(string.Format("v {0} {1} 0", end, Y));
                                            sr.WriteLine(string.Format("v {0} {1} {2}", end, Y, FLOOR_HEIGHT));
                                            sr.WriteLine(string.Format("v {0} {1} {2}", start, Y, FLOOR_HEIGHT));

                                            counter = WriteUnionPoint(sr, DOOR_POINTS, counter);
                                        }
                                        else
                                        {                                     
                                            counter = DrawWall(sr, wall, counter);   
                                        }
                                    }
                                }
                                else if(wall.fromPoint.x == wall.toPoint.x)
                                {
                                    
                                    int start = wall.fromPoint.y > wall.toPoint.y ? wall.toPoint.y : wall.fromPoint.y;
                                    int end = wall.fromPoint.y > wall.toPoint.y ? wall.fromPoint.y : wall.toPoint.y;

                                    int X = wall.fromPoint.x;
                                    if (wall.partitionWall.Count != 0)
                                    {
                                        wall.partitionWall.Sort();

                                        foreach (int point in wall.partitionWall)
                                        {
                                            int offset = (point - start - DOOR_WIDTH) / 2;
                                            if (point - start > DOOR_WIDTH)
                                            {
                                                sr.WriteLine(string.Format("v {0} {1} 0", X, start));
                                                sr.WriteLine(string.Format("v {0} {1} 0", X, start + offset));
                                                sr.WriteLine(string.Format("v {0} {1} {2}", X, start + offset, DOOR_HEIGHT));
                                                sr.WriteLine(string.Format("v {0} {1} {2}", X, start + offset + DOOR_WIDTH, DOOR_HEIGHT));
                                                sr.WriteLine(string.Format("v {0} {1} 0", X, start + offset + DOOR_WIDTH));
                                                sr.WriteLine(string.Format("v {0} {1} 0", X, point));
                                                sr.WriteLine(string.Format("v {0} {1} {2}", X, point, FLOOR_HEIGHT));
                                                sr.WriteLine(string.Format("v {0} {1} {2}", X, start, FLOOR_HEIGHT));

                                                start = point;

                                                counter = WriteUnionPoint(sr, DOOR_POINTS, counter);
                                            }
                                            else if(DOOR_WIDTH > start - point)
                                            {
                                                counter = DrawWall(sr, new Wall(new Point(X, start), new Point(X, point)), counter);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (end - start > DOOR_WIDTH)
                                        {
                                            sr.WriteLine(string.Format("v {0} {1} 0", X, start));
                                            int offset = (end - start - DOOR_WIDTH) / 2;
                                            sr.WriteLine(string.Format("v {0} {1} 0", X, start + offset));
                                            sr.WriteLine(string.Format("v {0} {1} {2}", X, start + offset, DOOR_HEIGHT));
                                            sr.WriteLine(string.Format("v {0} {1} {2}", X, start + offset + DOOR_WIDTH, DOOR_HEIGHT));
                                            sr.WriteLine(string.Format("v {0} {1} 0", X, start + offset + DOOR_WIDTH));
                                            sr.WriteLine(string.Format("v {0} {1} 0", X, end));
                                            sr.WriteLine(string.Format("v {0} {1} {2}", X, end, FLOOR_HEIGHT));
                                            sr.WriteLine(string.Format("v {0} {1} {2}", X, start, FLOOR_HEIGHT));

                                            counter = WriteUnionPoint(sr, DOOR_POINTS, counter);
                                        }
                                        else
                                        {
                                            counter = DrawWall(sr, wall, counter);
                                        }
                                    }                                                                      
                                }
                            }
                           
                        }

                    }
                }
            }

        }
    }
}

