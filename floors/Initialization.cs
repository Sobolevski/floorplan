﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using floorplan.Models;

namespace floorplan
{
    public class Initialization
    {
        public static List<Wall> CreateRoom(List<Point> points)
        {
            return points.Select((point, index) =>
            {
                var next = points[index == points.Count - 1 ? 0 : index + 1];
                return new Wall(point, next);
            }).ToList();
        }
    }
}
