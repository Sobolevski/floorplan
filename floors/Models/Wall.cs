﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace floorplan.Models
{
    public class Wall
    {
        public Point fromPoint;
        public Point toPoint;
        public List<int> partitionWall;
        public bool window;
        public bool isBorderWall;
        public bool EntranceDoor;

        public Wall(Point fromPoint, Point toPoint)
        {
            this.fromPoint = fromPoint;
            this.toPoint = toPoint;
            partitionWall = new List<int>();
        }
    }

}
