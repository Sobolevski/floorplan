﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace floorplan.Models
{
    public class Apartment
    {
        public List<Wall> border;
        public List<List<Wall>> rooms;
        public bool EntranceDoor;

        public Apartment(List<Wall> border_walls, List<List<Wall>> rooms)
        {
            border = border_walls;
            this.rooms = rooms;
        }

    }
}
