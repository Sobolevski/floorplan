﻿using System;
using System.IO;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using floorplan;
using floorplan.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace UnitTests
{
    [TestClass]
    public class TestPositioning
    {
        [TestMethod]
        public void TestWindows()
        {
            var apartments = Mocks.Apartments;
            floorplan.Program.Main(apartments);

            Assert.AreEqual(apartments[0].rooms[0][0].window, true);
            Assert.IsTrue(apartments[4].rooms[7][3].window);
            Assert.IsFalse(apartments[4].rooms[7][0].window);
            Assert.AreEqual(apartments[0].rooms[1][1].window, false);
            Assert.AreEqual(apartments[0].rooms[5][4].window, false);
            Assert.AreEqual(apartments[0].rooms[5][5].window, true);
            Assert.AreEqual(apartments[0].rooms[0][0].window, true);

        }

        [TestMethod]
        public void Initialization()
        {
            var apartments = Mocks.Apartments;
            floorplan.Program.Main(apartments);

            ObjWriter.WriteApartment(apartments);
        }

        [TestMethod]
        public void TestBorderWall()
        {
            var apartments = Mocks.Apartments;
            floorplan.Program.Main(apartments);

            Assert.AreEqual(apartments[0].rooms[5][4].isBorderWall, false);
            Assert.IsTrue(apartments[3].rooms[5][3].isBorderWall);
            Assert.IsTrue(apartments[3].rooms[0][3].isBorderWall);
            Assert.IsTrue(apartments[3].rooms[6][3].isBorderWall);
            Assert.IsTrue(apartments[3].rooms[6][3].isBorderWall);
            Assert.IsTrue(apartments[3].rooms[5][3].isBorderWall);
        }

        [TestMethod]
        public void TestFindPartition()
        {
            var apartments = Mocks.Apartments;
            floorplan.Program.Main(apartments);

            Assert.AreEqual(apartments[4].rooms[6][1].partitionWall[0], 18000);
            Assert.AreEqual(apartments[4].rooms[5][2].partitionWall[0], 3000);
            Assert.AreEqual(apartments[0].rooms[4][7].partitionWall[0], 6000);
            Assert.AreEqual(apartments[0].rooms[0][3].partitionWall[0], 4000);
            Assert.AreEqual(apartments[2].rooms[2][0].partitionWall[0], 16000);
            Assert.AreEqual(apartments[2].rooms[4][3].partitionWall[0], 8500);
        }
    }
}
