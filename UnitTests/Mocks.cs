﻿using floorplan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    public static class Mocks
    {
        public static readonly List<Apartment> Apartments;

        static Mocks()
        {
            List<Wall> room_zero = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(0, 4500), new Point(0, 7500), new Point(5000, 7500), new Point(5000, 4500) });

            List<Wall> room_one = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(6000, 0), new Point(6000, 3000), new Point(10500, 3000), new Point(10500, 0) });

            List<Wall> room_two = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(5000, 4500), new Point(5000, 7500), new Point(7500, 7500), new Point(7500, 4500) });

            List<Wall> room_three = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(8500, 3000), new Point(8500, 6000), new Point(10500, 6000), new Point(10500, 3000) });

            List<Wall> room_four = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(4000, 3000), new Point(4000, 4500), new Point(7500, 4500), new Point(7500, 7500),
            new Point(9500, 7500), new Point(9500, 6000), new Point(8500, 6000), new Point(8500, 3000)});

            List<Wall> room_five = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(0, 0), new Point(0, 4500), new Point(4000, 4500), new Point(4000, 3000), new Point(6000, 3000), new Point(6000, 0) });

            List<Wall> border_apartment = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(0, 0), new Point(0, 7500), new Point(9500, 7500),
                new Point(9500, 6000), new Point(10500, 6000), new Point(10500, 0) });

            Apartment apartment = new Apartment(border_apartment, new List<List<Wall>>() { room_zero, room_one, room_two, room_three, room_four, room_five });



            List<Wall> roomZero = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(0, 7500), new Point(0, 11000), new Point(4000, 11000), new Point(4000, 7500) });

            List<Wall> roomOne = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(0, 11000), new Point(0, 14000), new Point(6200, 14000), new Point(6200, 11000) });

            List<Wall> roomTwo = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(6200, 14000), new Point(7200, 14000), new Point(7200, 9500), new Point(4000, 9500), new Point(4000, 11000), new Point(6200, 11000) });

            List<Wall> roomThree = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(4000, 7500), new Point(4000, 9500), new Point(7200, 9500), new Point(7200, 7500) });

            List<Wall> borderOneApart = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(0, 7500), new Point(0, 14000), new Point(7200, 14000), new Point(7200, 7500) });

            Apartment apartmentOne = new Apartment(borderOneApart, new List<List<Wall>>() { roomZero, roomOne, roomTwo, roomThree });



            List<Wall> roomone = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(0, 14000), new Point(0, 21500), new Point(3000, 21500), new Point(3000, 18500),
            new Point(6000, 18500), new Point(6000, 16000), new Point(3500, 16000), new Point(3500, 14000)});

            List<Wall> roomtwo = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(3000, 18500), new Point(3000, 21500), new Point(7500, 21500), new Point(7500, 18500) });

            List<Wall> roomthree = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(6000, 14000), new Point(6000, 18500), new Point(7500, 18500), new Point(7500, 16000),
                new Point(8500, 16000), new Point(8500, 14000)});

            List<Wall> roomfour = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(3500, 14000), new Point(3500, 16000), new Point(6000, 16000), new Point(6000, 14000) });

            List<Wall> roomfive = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(7500, 16000), new Point(7500, 21500), new Point(10500, 21500), new Point(10500, 16000) });

            List<Wall> roomsix = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(8500, 14000), new Point(8500, 16000), new Point(10500, 16000), new Point(10500, 14000) });

            List<Wall> borderTwoApart = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(0, 14000), new Point(0, 21500), new Point(10500, 21500), new Point(10500, 14000), new Point(7200, 14000) });

            Apartment apartmentTwo = new Apartment(borderTwoApart, new List<List<Wall>>() { roomone, roomtwo, roomthree, roomfour, roomfive, roomsix });



            List<Wall> RoomOne = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(13700, 10750), new Point(13700, 14000), new Point(16000, 14000), new Point(16000, 10750) });

            List<Wall> RoomTwo = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(10500, 14000), new Point(10500, 21500), new Point(16000, 21500), new Point(16000, 14000) });

            List<Wall> RoomThree = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(16000, 18500), new Point(16000, 21500), new Point(21000, 21500), new Point(21000, 18500) });

            List<Wall> RoomFour = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(16000, 16000), new Point(16000, 18500), new Point(21000, 18500), new Point(21000, 16000) });

            List<Wall> RoomFive = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(16000, 13000), new Point(16000, 16000), new Point(18000, 16000), new Point(18000, 13000) });

            List<Wall> RoomSix = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(18000, 10750), new Point(18000, 16000), new Point(21000, 16000), new Point(21000, 10750) });

            List<Wall> RoomSeven = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(16000, 10750), new Point(16000, 13000), new Point(18000, 13000), new Point(18000, 10750) });

            List<Wall> Border_two = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(10500, 14000), new Point(10500, 21500), new Point(21000, 21500), new Point(21000, 10750), new Point(13700, 10750), new Point(13700, 14000) });

            Apartment apartmentThree = new Apartment(Border_two, new List<List<Wall>>() { RoomOne, RoomTwo, RoomThree, RoomFour, RoomFive, RoomSix, RoomSeven });



            List<Wall> lastOne = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(13700, 7500), new Point(13700, 10750), new Point(16000, 10750), new Point(16000, 7500) });

            List<Wall> lastTwo = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(16000, 7500), new Point(16000, 10750), new Point(18000, 10750), new Point(18000, 7500) });

            List<Wall> lastThree = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(18000, 6000), new Point(18000, 10750), new Point(21000, 10750), new Point(21000, 6000) });

            List<Wall> lastFour = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(11500, 6000), new Point(11500, 7500), new Point(13700, 7500), new Point(13700, 6000) });

            List<Wall> lastSix = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(13700, 6000), new Point(13700, 7500), new Point(18000, 7500), new Point(18000, 6000) });

            List<Wall> lastSeven = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(10500, 0), new Point(10500, 6000), new Point(16000, 6000), new Point(16000, 0) });

            List<Wall> lastEight = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(16000, 3000), new Point(16000, 6000), new Point(21000, 6000), new Point(21000, 3000) });

            List<Wall> lastNine = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(16000, 0), new Point(16000, 3000), new Point(21000, 3000), new Point(21000, 0) });

            List<Wall> borderFive = floorplan.Initialization.CreateRoom(new List<Point>() { new Point(10500, 0), new Point(10500, 6000), new Point(11500,6000), new Point(11500, 7500),
            new Point(13700, 7500), new Point(13700, 10750), new Point(21000, 10750), new Point(21000, 0)});

            Apartment apartmentLast = new Apartment(borderFive, new List<List<Wall>>() { lastOne, lastTwo, lastThree, lastFour, lastSix, lastSeven, lastEight, lastNine });

            Apartments = new List<Apartment>() { apartment, apartmentOne, apartmentTwo, apartmentThree, apartmentLast };
        }



    }
}
